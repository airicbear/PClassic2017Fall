import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Perfect {
  /* DO NOT MODIFY BELOW THIS LINE*/
  public static void main(String[] args) {
    try {
      BufferedReader br = new BufferedReader(new FileReader("PerfectIN.txt"));
      while (br.ready()) {
        System.out.println(isPerfect(Integer.parseInt(br.readLine())));
      }
      br.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  /* DO NOT MODIFY ABOVE THIS LINE*/

  public static boolean isPerfect(long n) {
    // YOUR CODE HERE
	  ArrayList<Long> divisors = new ArrayList<Long>();
	  for(long k = 1L; k < n; k++) {
		  if(n % k == 0) {
			  divisors.add(k);
		  }
	  }
	  long sum = 0;
	  for(long divisor : divisors)
	  {
		  sum += divisor;
	  }
    return sum == n;
  }
}
