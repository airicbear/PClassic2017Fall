import java.util.*;
import java.io.*;

class ParkingSpots {
	
	/* DO NOT MODIFY BELOW THIS LINE */ 
    private static class Neighborhood {
	    int start;
	    int end;
	    Neighborhood() { start = 0; end = 0; }
	    Neighborhood(int s, int e) { start = s; end = e; }

	    public String toString() {
	    	return "[" + this.start + ", " + this.end + "]";
	    }
    }

    
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("ParkingSpotsIN.txt"));
		while (sc.hasNextLine()) {
			int N = sc.nextInt();
			List<Neighborhood> neighborhoods = new ArrayList<Neighborhood>();
			for (int i = 0; i < N; i++) {
				int s = sc.nextInt(), e = sc.nextInt();
				Neighborhood interval = new Neighborhood(s, e);
				neighborhoods.add(interval);
			}
			System.out.println(merge(neighborhoods));
		}
	}
	/* DO NOT MODIFY ABOVE THIS LINE*/

	public static List<Neighborhood> merge(List<Neighborhood> neighborhoods) {
		List<Neighborhood> street = new ArrayList<Neighborhood>();
		int count = 0;
    	for(int i = 0; i < neighborhoods.size(); i++)
    	{
    		int start1 = neighborhoods.get(count).start;
    		int end1 = neighborhoods.get(count).end;
    		int start2 = neighborhoods.get(count+1).start;
    		int end2 = neighborhoods.get(count+1).end;
    		Neighborhood currentHood = neighborhoods.get(i);
    		if(end1 >= start2)
    		{
    			street.add(new Neighborhood(start1, end2));
    			i++;
    		}
    		else
    		{
    			street.add(currentHood);
    		}
    		count++;
    	}
    	int count1 = 0;
    	for(int i = 0; i < street.size(); i++)
    	{
    		if(street.get(count1).end >= street.get(count1+1).start)
        	{
        		street.set(count1, new Neighborhood(street.get(count1).start, street.get(count1+1).end));
        		street.remove(street.size()-(i+1));
        		count1++;
        	}
    	}
    	return street;
    }

}